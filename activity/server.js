const express = require("express");
const app = express();
const port = 4000;
const mongoose = require("mongoose");

// Connecting to MongoDB
mongoose.connect(
	"mongodb+srv://admin:admin@b145.m0v7t.mongodb.net/session32?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

let db = mongoose.connection;
db.on(
	"error",
	console.error.bind(console, "There is an error with the connection")
);
db.once("open", () => console.log(`Successfully connected to the database`));

// Middelwares
app.use(express.json());
app.use(
	express.urlencoded({
		extended: true,
	})
);

// Mongoose Schemas
// schema === blueprint of your data

const userSchema = new mongoose.Schema({
	email: String,
	username: String,
	password: String,
	age: Number,
	isAdmin: {
		type: Boolean,
		default: "false",
	},
});

// MODEL=============
const User = mongoose.model("User", userSchema);

// BUSINESS LOGIC====================

// REGISTER USER
app.post("/users/signup", (req, res) => {
	User.findOne({ email: req.body.email }, (err, result) => {
		if (result != null && result.email === req.body.email) {
			return res.send(`Duplicate email found: ${err}`);
		} else {
			let newUser = new User({
				email: req.body.email,
				username: req.body.username,
				password: req.body.password,
				age: req.body.age,
			});
			newUser.save((saveErr, savedUser) => {
				if (saveErr) {
					return console.error(saveErr);
				} else {
					return res
						.status(200)
						.send(`New user created: ${savedUser}`);
				}
			});
		}
	});
});

// RETRIEVE ALL USERS
app.get("/users", (req, res) => {
	User.find({}, (err, result) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				users: result,
			});
		}
	});
});

// UPDATING USERS
app.put("/users/update/:wildcard", (req, res) => {
	let userId = req.params.wildcard;
	let username = req.body.username;
	User.findByIdAndUpdate(
		userId,
		{ username: username },
		(err, updatedUser) => {
			if (err) {
				console.log(err);
			} else {
				res.send(
					`Congratulations! username "${username}" has been updated.`
				);
			}
		}
	);
});

// DELETE A USER
app.delete("/users/archive-user/:wildcard", (req, res) => {
	let userId = req.params.wildcard;
	User.findByIdAndDelete(userId, (err, deletedUser) => {
		if (err) {
			console.log(err);
		} else {
			let userName = deletedUser.username;
			res.send(`${userName} has been deleted`);
		}
	});
});

app.listen(port, () => console.log(`Server running at port ${port}`));
