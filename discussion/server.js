const express = require("express");
const app = express();
const port = 4000;
const mongoose = require("mongoose");

// Connecting to MongoDB
mongoose.connect(
	"mongodb+srv://admin:admin@b145.m0v7t.mongodb.net/session32?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

let db = mongoose.connection;
db.on(
	"error",
	console.error.bind(console, "There is an error with the connection")
);
db.once("open", () => console.log(`Successfully connected to the database`));

// Middelwares
app.use(express.json());
app.use(
	express.urlencoded({
		extended: true,
	})
);

// Mongoose Schemas
// schema === blueprint of your data

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending",
	},
});

// Model
const Task = mongoose.model("Task", taskSchema);

// Business Logic

// Creating a new task
app.post("/tasks", (req, res) => {
	Task.findOne({ name: req.body.name }, (err, result) => {
		if (result != null && result.name === req.body.name) {
			return res.send(`Duplicate task found: ${err}`);
		} else {
			let newTask = new Task({
				name: req.body.name,
			});
			newTask.save((saveErr, savedTask) => {
				if (saveErr) {
					return console.error(saveErr);
				} else {
					return res
						.status(200)
						.send(`New task created: ${savedTask}`);
				}
			});
		}
	});
});

// Getting all tasks
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				tasks: result,
			});
		}
	});
});

// Updating a Task name
app.put("/tasks/update/:taskId", (req, res) => {
	let taskId = req.params.taskId;
	let name = req.body.name;
	Task.findByIdAndUpdate(taskId, { name: name }, (err, updatedTask) => {
		if (err) {
			console.log(err);
		} else {
			res.send(`Congratulations the task "${name}" has been updated`);
		}
	});
});

// Delete a Task
app.delete("/tasks/archive-task/:taskId", (req, res) => {
	let taskId = req.params.taskId;
	Task.findByIdAndDelete(taskId, (err, deletedTask) => {
		if (err) {
			console.log(err);
		} else {
			res.send(`${deletedTask} has been deleted`);
		}
	});
});
app.listen(port, () => console.log(`Server running at port ${port}`));
